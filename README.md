# Nyanko

Wrap `netcat` command

## Installation

You will need `netcat`, `pv` before install this gem

Run command:

    $ gem install nyanko

## Usage

    nyanko command PORT EXECUTABLE       # bind port to executable
    nyanko get IP_ADDRESS PORT FILENAME  # get files from server
    nyanko help [COMMAND]                # Describe available commands or one specific command
    nyanko raw IP_ADDRESS PORT           # create raw connection
    nyanko serve PORT FILENAME           # act as server and serve file

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
