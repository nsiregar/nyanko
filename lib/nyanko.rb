# frozen_string_literal: true

require 'nyanko/version'
require 'resolv'

module Nyanko
  class Error < StandardError; end

  class Nyan
    MAX_PORT_NUMBER = 65_535
    MIN_PORT_NUMBER = 1024

    def self.validate_localport(port)
      raise Error, "#{port} is not a number" unless port =~ /^[0-9]+$/

      port = port.to_i

      if port > MAX_PORT_NUMBER
        raise Error, "#{port} larger than #{MAX_PORT_NUMBER}"
      end

      uid = Process.uid
      return if uid.zero?
      return unless port < MIN_PORT_NUMBER

      raise Error, "#{port} not between #{MIN_PORT_NUMBER} and #{MAX_PORT_NUMBER}"
    end

    def self.validate_ip(ip_addr)
      return if ip_addr =~ Resolv::IPv4::Regex

      raise Error, "#{ip_addr} is invalid IPv4"
    end
  end
end
