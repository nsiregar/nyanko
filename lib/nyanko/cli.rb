require 'thor'
require 'nyanko'
require 'digest'

class Nyanko::CLI < Thor
  def self.exit_on_failure?
    true
  end

  desc 'get IP_ADDRESS PORT FILENAME', 'get files from server'
  def get(ip_addr, port, filename)
    Nyanko::Nyan.validate_ip ip_addr
    Nyanko::Nyan.validate_localport port

    system("nc #{ip_addr} #{port} | pv -rb > #{filename}")
  rescue Nyanko::Error => e
    warn e.message
    exit 1
  end

  desc 'serve PORT FILENAME', 'act as server and serve file'
  def serve(port, filename)
    Nyanko::Nyan.validate_localport port

    sha256 = Digest::SHA256.file filename
    sha256sum = sha256.hexdigest

    puts "File checksum: #{sha256sum}"
    puts "Serve file in port #{port}"

    system("cat #{filename} | pv -rb | nc -l -p #{port}")
  rescue SystemExit, Interrupt
    exit 1
  rescue Nyanko::Error => e
    warn e.message
    exit 1
  end

  desc 'raw IP_ADDRESS PORT', 'create raw connection'
  def raw(ip_addr, port)
    Nyanko::Nyan.validate_ip ip_addr
    Nyanko::Nyan.validate_localport port

    system("nc #{ip_addr} #{port}")
  rescue Nyanko::Error => e
    warn e.message
    exit 1
  end

  desc 'command PORT EXECUTABLE', 'bind port to executable'
  def command(port, executable)
    Nyanko::Nyan.validate_localport port

    system("nc -l -p #{port} -e #{executable}")
  rescue Nyanko::Error => e
    warn e.message
    exit 1
  end
end
