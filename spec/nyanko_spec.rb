# frozen_string_literal: true

RSpec.describe Nyanko do
  it 'has a version number' do
    expect(Nyanko::VERSION).not_to be nil
  end
end

RSpec.describe Nyanko::Nyan do
  context '.validate_ip' do
    it 'validates ipv4 address' do
      ip_addr = '192.168.1.1'
      expect do
        described_class.validate_ip ip_addr
      end.not_to raise_error
    end
  end

  context '.validate_localport' do
    it 'validates localport' do
      port = '1025'

      expect do
        described_class.validate_localport port
      end.not_to raise_error
    end

    context 'when port not an integer' do
      it 'raises error port is not a number' do
        port = '123'

        expect do
          described_class.validate_localport port
        end.to raise_error(Nyanko::Error)
      end
    end

    context 'when port number larger than 65535' do
      it 'raises error' do
        port = '65536'

        expect do
          described_class.validate_localport port
        end.to raise_error(Nyanko::Error)
      end
    end

    context 'when process uid is not zero' do
      context 'when port less than 1024' do
        it 'raises error' do
          port = '1023'
          binding.irb

          expect do
            described_class.validate_localport port
          end.to raise_error(Nyanko::Error)
        end
      end
    end
  end
end
